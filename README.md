## Synopsis

Build a docker image containing doxygen. It is based on the
ci-web-deployer image, which in turn is based on cc7-base, and
contains the scripts needed to deploy to EOS/DFS.

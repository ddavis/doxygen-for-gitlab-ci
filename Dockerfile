FROM gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest

MAINTAINER ddavis@cern.ch

# Install pip and make
RUN /usr/bin/yum install -y doxygen
